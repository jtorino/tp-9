#include "BitwsieFunctions.h"

#define IS8BIT(x)   ( (x) >=0 && (x) <=7 ? 1 : 0 )
#define IS16BIT(x)  ( (x) >=0 && (x) <=15 ? 1 : 0 )
#define ISLETTER(x) ( (x) >= 'A' && (x) <= 'Z' ||(x) >= 'a' && (x) <= 'z' )
#define MAYUS(x)    ( (x) >= 'A' && (x) <= 'Z' ? (x) + ('a' - 'A' ): (x) )

static ports puertos = { 0 , 0 };

void BitSet(char portLetter, const int bitNum )
{
    if (ISLETTER(portLetter))
    {
        portLetter = MAYUS(portLetter);
        switch (portLetter)
        {
            case 'a':
                puertos.by.puertoA = puertos.by.puertoA | Create8bitMask(bitNum);
                break;
            case 'b':
                puertos.by.puertoB = puertos.by.puertoB | Create8bitMask(bitNum);
                break;
            case 'd':
                puertos.joint.puertoD = puertos.joint.puertoD | Create16bitMask(bitNum);
                
        }
        
    }
    
    
}

void BitClr( char portLetter , const int bitNum )
{
            
    if (ISLETTER(portLetter))
    {
        portLetter = MAYUS(portLetter);
        {
            switch (portLetter)
            {
                case 'a':
                    puertos.by.puertoA = puertos.by.puertoA & ~(1 << (bitNum));
                    break;
                case 'b':
                    puertos.by.puertoB = puertos.by.puertoB & ~(1 << (bitNum));
                    break;
                case 'd':
                    if (bitNum >= 8)
                    {
                        puertos.by.puertoA = puertos.by.puertoA & ~(1 << (bitNum - 7));
                    }
                    else
                    {
                        puertos.by.puertoB = puertos.by.puertoB & ~(1 << (bitNum));
                    }
            }
        }
    }
}

char BitGet (char portLetter , const int bitNum)
{
    char result;
    if (ISLETTER(portLetter))
    {
        portLetter = MAYUS(portLetter);
        {
            switch (portLetter)
            {
                case 'a':
                    if ((uint16_t)puertos.by.puertoA & Create8bitMask(bitNum))
                    {
                        result = 1;
                    }
                    else
                    {
                        result = 0;
                    }
                    break;
                case 'b':
                    if ((uint16_t)puertos.by.puertoB & Create8bitMask(bitNum))
                    {
                        result = 1;
                    }
                    else
                    {
                        result = 0;
                    }
                    break;
                case 'd':
                    if (puertos.joint.puertoD & Create16bitMask(bitNum))
                    {
                        result = 1;
                    }
                    else
                    {
                        result = 0;
                    }
                    break;
            }
            
        }
    }
    return result;
}

void BitToggle (char portLetter , const int bitNum)
{
    if (ISLETTER(portLetter))
    {
        portLetter = MAYUS(portLetter);
        switch (portLetter)
        {
            case 'a':
                puertos.by.puertoA = puertos.by.puertoA ^ Create8bitMask(bitNum);
                break;
            case 'b':
                puertos.by.puertoB = puertos.by.puertoB ^ Create8bitMask(bitNum);
                break;
            case 'd':
                puertos.joint.puertoD = puertos.joint.puertoD ^ Create16bitMask(bitNum);
                
        }
        
    } 
}

void MaskOn( char portLetter , const uint16_t mask)
{
    if (ISLETTER(portLetter))
    {
        portLetter = MAYUS(portLetter);
        switch (portLetter)
        {
            case 'a':
                puertos.by.puertoA = puertos.by.puertoA | mask;
                break;
            case 'b':
                puertos.by.puertoB = puertos.by.puertoB | mask;
            case 'd':
                puertos.joint.puertoD = puertos.joint.puertoD | mask;
                
        }
        
    } 
    
    return;
}

void MaskOff( char portLetter , const uint16_t mask )
{
    if (ISLETTER(portLetter))
    {
        portLetter = MAYUS(portLetter);
        switch (portLetter)
        {
            case 'a':
                puertos.by.puertoA = puertos.by.puertoA & ~mask;
                break;
            case 'b':
                puertos.by.puertoB = puertos.by.puertoB & ~mask;
            case 'd':
                puertos.joint.puertoD = puertos.joint.puertoD & ~mask;
                
        }
        
    } 
    return;
}

void MaskToggle(char portLetter , const uint16_t mask)
{
    if (ISLETTER(portLetter))
    {
        portLetter = MAYUS(portLetter);
        switch (portLetter)
        {
            case 'a':
                puertos.by.puertoA = puertos.by.puertoA ^ mask;
                break;
            case 'b':
                puertos.by.puertoB = puertos.by.puertoB ^ mask;
            case 'd':
                puertos.joint.puertoD = puertos.joint.puertoD ^ mask;
                
        }
        
    } 
    
    return;
}

uint8_t Create8bitMask( const int bitNum )
{
    uint8_t mask = 0b00000001;

    mask = mask << bitNum;
    
    return mask;
}

uint16_t Create16bitMask( const int bitNum )
{
    uint16_t mask = 0b0000000000000001;
    
    mask = mask << bitNum;
    
    return mask;
}