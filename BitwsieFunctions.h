#include <stdio.h>
#include "byte.h"

void BitSet(char portLetter, const int bitNum );
/* Recibe una letra que indica el puerto y el numero 
 * de bit que desea encender (del 0 al 7) */

void BitClr( char portLetter , const int bitNum );
/* Recibe una letra que indica el puerto y el numero
 * de bit que desea apagar (del 0 al 7) */

char BitGet (char portLetter , const int bitNum);
/* Recibe una letra que indica el puerto y el numero
 * de bit que desea recuperar, y devuelve un char
 * con valor en ascii 1 o 0 segun el estado del bit */

void BitToggle (char portLetter , const int bitNum);
/* Recibe una letra que indica el puerto y el numero
 * de bit que desea cambiar (del 0 al 7) */

void MaskOn( char portLetter , const uint16_t mask);
/* Recibe una letra que indica el puerto y una 
 * mascara la cual desea aplicar sobre el puerto 
 (enciende todos los bits de la mascara)*/

void MaskOff( char portLetter , const uint16_t mask );
/* Recibe una letra que indica el puerto y una 
 * mascara la cual desea aplicar sobre el puerto 
 (apaga todos los bits de la mascara)*/

void MaskToggle(char portLetter , const uint16_t mask);
/* Recibe una letra que indica el puerto y una 
 * mascara la cual desea aplicar sobre el puerto 
 (cambia todos los bits de la mascara)*/

uint8_t Create8bitMask( const int bitNum );
/* Recibe un numero de bit y rea una mascara de 8 bits
 * con todos 0 salvo el bit en la posicion indicada*/

uint16_t Create16bitMask( const int bitNum );
/* Recibe un numero de bit y rea una mascara de 16 bits
 * con todos 0 salvo el bit en la posicion indicada*/
