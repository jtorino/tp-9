#include <stdio.h>
#include "BitwsieFunctions.h"

void Estado();

int main(void)
{
        int exit;
        char c;
        
        printf("Puerto A:\n");
        Estado();
        
        while (exit!=1)
        {
	
            while ( (c=getchar() ) !='\n')
            {

		if (c =='q')				//verifica que no termino el progama.
		{
			exit=1;
		}
		else if (c>='0' && c<='7')		//verifica si se solicito encender un led.
		{
			BitSet ('a',(c-'0'));
                        Estado();
		}

		else if (c=='c')			//apaga todos los leds.
		{
			MaskOff('a',0xFF);					
                        Estado();
                }
			
		else if (c=='s')			//enciende todos los leds.
		{
			MaskOn('a',0xFF);					
                        Estado();
                }

		else if (c=='t')			//invierte el estado de cada led.
		{
			MaskToggle('a',0xFF);				
                        Estado();
                }


	}
        }
    return 0;
}

void Estado()						//actualiza el estado de los leds del porta.
{
    printf ("LED 0: %d\t", BitGet('a',0));
    printf ("LED 1: %d\t", BitGet('a',1));
    printf ("LED 2: %d\t", BitGet('a',2));
    printf ("LED 3: %d\t", BitGet('a',3));
    printf ("LED 4: %d\t", BitGet('a',4));
    printf ("LED 5: %d\t", BitGet('a',5));
    printf ("LED 6: %d\t", BitGet('a',6));
    printf ("LED 7: %d\t\n", BitGet('a',7));   
    
    return;
}
