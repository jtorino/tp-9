#include <stdint.h>

typedef struct
{
    /* Se define el struct que contiene a los puertos A y B*/
    
    uint8_t puertoB;
    uint8_t puertoA;
    
} BYTE_REG;

typedef struct
{
    /*Se define el struct que define al Puerto D. Se pordria hacer
     Si otro struct pero para mantener la consistencia se prefirio
     realizar de esta manera*/
    
    uint16_t puertoD;
    
} DOUBLEBYTE_REG;

typedef union
{
    /* Se crea una union con ambos structs de tal manera que el 
     puerto D ocupe los mismos bytes que los puertos A y B 
     combinados*/
    
    BYTE_REG by;
    DOUBLEBYTE_REG joint;
    
}ports;
